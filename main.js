
//load our libraries
var express = require("express");
var path = require("path");

//create an instance of express application
var app = express();

//Define routes
app.use(express.static(__dirname + "/public"));
//console.log(">>__dirname: " + __dirname);

app.use(function(req,resp) {
    resp.status(404);
    resp.type("text/html");
    resp.send("<h1>file not found </h1><p>The current time is " + new Date() + "</p>");
});

//setting the port as a property of the app
app.set("port", 3000);

console.info("port: "+ app.get("port"));

app.listen(app.get("port"), function() {
console.info("Application is listening on port" + app.get("port"));
console.info("yaaaah..");
});
